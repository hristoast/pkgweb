# pkgweb

Code for generating a web interface to Portmod's package repositories.

## Dependencies

- Python, portmod and jinja2 (https://pypi.org/project/Jinja2/)
- Soupault: https://soupault.app/#downloads
- Stork: https://stork-search.net/docs/install
- Some HTTP server (Nginx, etc)

For convenience, the `output.sh` script will download missing dependencies automatically (for python dependencies, this requires virtualenv).

## Building And Running

1. Get a local copy of [the OpenMW-Mods repo](https://gitlab.com/portmod/openmw-mods).
1. Run `output.sh`. The resulting site will be created in a directory called `build`.

        cd ~/src/pkgweb
        ./output.sh ../openmw-mods http://localhost MyPkgWeb /pkgweb

1. In order for the search feature to function, you should serve this from a webserver. This is a sample Nginx server configuration:

        server {
          listen 80;
          server_name localhost;
          location ^~ /pkgweb {
            alias /home/username/src/pkgweb/build;
          }
        }

1. Open http://localhost/pkgweb in a browser to view your local site.
